import React from 'react'

export const Tela = ({ value }) => <input value={value || '0'} disabled />
