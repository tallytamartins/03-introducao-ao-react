import React, { useState } from 'react'
import { Tela } from './components/Tela'
import './App.css'


function App() {
  const [tela, setTela] = useState('')

  const adicionarCaracter = numero => setTela(String(tela) + String(numero))

  const igualdade = () => setTela(eval(tela))

    return (
    <div id="calculadora">
      <div id="tela">
        <Tela value={tela} disabled />
      </div>
          
          
      <div id="teclas">
  
  
        <button onClick={() => setTela('')}>C</button>
        <button onClick={() => igualdade()}>=</button>
        <button onClick={() => adicionarCaracter("/")}>/</button>
        <button onClick={() => adicionarCaracter("*")}>*</button>
        <button onClick={() => adicionarCaracter("-")}>-</button>
        <button onClick={() => adicionarCaracter("+")}>+</button>
        <button onClick={() => adicionarCaracter(9)}>9</button>
        <button onClick={() => adicionarCaracter(8)}>8</button>
        <button onClick={() => adicionarCaracter(7)}>7</button>
        <button onClick={() => adicionarCaracter(6)}>6</button>
        <button onClick={() => adicionarCaracter(5)}>5</button>
        <button onClick={() => adicionarCaracter(4)}>4</button>
        <button onClick={() => adicionarCaracter(3)}>3</button>
        <button onClick={() => adicionarCaracter(2)}>2</button>
        <button onClick={() => adicionarCaracter(1)}>1</button>
        <button onClick={() => adicionarCaracter(0)}>0</button>
      </div>
    </div>
  )
}

export default App
